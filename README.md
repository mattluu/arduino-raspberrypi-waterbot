# This is my README

This project is my attempt to build a smart garden for my small apartment. The purpose of the project was to have some type of automated system that would help keep my outside garden watered while I was away for the weekends. (I commute long distance over the weekends.) 

I've also been tinkering with small computing devices and wanted to combine a couple different systems together in order to build a platform that I could use as a starting place for other projects in the future. This setup is basically an Arduino that interfaces with a Raspberry Pi.

The pretty graphs and more details about the project can be found on my personal website, http://www.mattluurocks.com/waterbot/

--Matt