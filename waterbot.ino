/*
  matts waterbot
  Turns on a water pump on for ten seconds, then off for 23 hours 59 minutes and 50 seconds, repeatedly.
 
  will implement more sensors later
  
  TODO
  add photocell to determine when light is off, then water during that period
  add soil sensors, only water if sensors are below certain threshold
  tweet actions
  upload stats to webpage
  
 */

//--------------------------------
//    Timer stuff
//--------------------------------
// including the library for the timer
#include "Timer.h"
// defining the timer
Timer t;
//--------------------------------
//    DHT stuff
//--------------------------------
// including the library for DHT22
#include "DHT.h"
// defining the DHT22 pin connection
#define DHTPIN 2

// defining the type of DHT sensor we are using
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

//declaring DHT type
DHT dht(DHTPIN, DHTTYPE);
//--------------------------------
//  Photocell Stuff
//--------------------------------
// select the input pin for the photocell
int photoPin = 0;
// variable to store the value coming from the photocell
int photoValue = 0;
const int threshold = 400;
//--------------------------------
//  WaterPump/Relay Stuff
//--------------------------------
// Pin 6 has waterpump connected 
int pump = 10;
int pumpOn;

void setup()
{
  Serial.begin(9600);
  dht.begin(); //initialize the DHT sensor
  // poll the sensor ever 60 seconds
  int pollEvent = t.every(60000, pollData);
  //Serial.print("Data Polling started. Event ID#");
  //Serial.println(pollEvent);
  
  
  // run every approx 3 hours
  int waterEvent = t.every(10000000, relayTrigger);
  // run ever 30 seconds for testing purposes
  //int waterEvent = t.every(30000, relayTrigger);
  //Serial.print("Watering service started. Event ID#");
  //Serial.println(waterEvent); 
  // first run 5 seconds after reset
  t.after(5000, relayTrigger);
  
  // setting the pump pin to output mode
  pinMode(pump, OUTPUT); 
  
}

void loop()
{
  t.update();
}

void pollData()
{
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float hum = dht.readHumidity();
  float temp = dht.readTemperature();
  photoValue = analogRead(photoPin);

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
if (isnan(temp) || isnan(hum)) {
    Serial.print("0.00,");
    Serial.print("0.00,");
    Serial.print(photoValue);
    Serial.print(",0\n");
  } else {
    //Serial.print("Humidity: "); 
    Serial.print(hum);
    Serial.print(",");
    //Serial.print("Temperature: "); 
    Serial.print(temp);
    Serial.print(",");
    //Serial.print("Light Value: ");
    Serial.print(photoValue);
    Serial.print(",0\n");
  }
}

void relayStop() {
  
  float hum = dht.readHumidity();
  float temp = dht.readTemperature();
  photoValue = analogRead(photoPin);
  
  //Serial.print("Humidity: "); 
  Serial.print(hum);
  Serial.print(",");
  //Serial.print("Temperature: "); 
  Serial.print(temp);
  Serial.print(",");
  //Serial.print("Light Value: ");
  Serial.print(photoValue);
  Serial.print(",0\n");
  digitalWrite(pump,LOW);
}


void relayTrigger()
{

  float hum = dht.readHumidity();
  float temp = dht.readTemperature();
  photoValue = analogRead(photoPin);
   
  if ( photoValue < threshold ) {
    //Serial.print("Humidity: "); 
    Serial.print(hum);
    Serial.print(",");
    //Serial.print("Temperature: "); 
    Serial.print(temp);
    Serial.print(",");
    //Serial.print("Light Value: ");
    Serial.print(photoValue);
    Serial.print(",1\n");
    digitalWrite(pump,HIGH);
    t.after(10000, relayStop);
  }else{
    //Serial.print("Humidity: "); 
    Serial.print(hum);
    Serial.print(",");
    //Serial.print("Temperature: "); 
    Serial.print(temp);
    Serial.print(",");
    //Serial.print("Light Value: ");
    Serial.print(photoValue);
    Serial.print(",0\n");
    digitalWrite(pump,LOW);
  }  
}