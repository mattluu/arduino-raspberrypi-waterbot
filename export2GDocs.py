#!/usr/bin/python

import time
import gdata.spreadsheet.service
import csv

email = 'your@gmail.com'
password = 'password'


Col1 = "ColumnName1"
Col2 = "ColumnName2"
Col3 = "ColumnName3"
Col4 = "ColumnName4"
mydictionary={Col1:[], Col2:[], Col3:[], Col4:[]}
csvFile = csv.reader(open("data.log", "rb"))
for row in csvFile:
  mydictionary[Col1].append(row[0])
  mydictionary[Col2].append(row[1])
  mydictionary[Col3].append(row[2])
  mydictionary[Col4].append(row[3])
  
humidity = (row[0])
temperature = (row[1])
light = (row[2])
pumpstate = (row[3])


# Find this value in the url with 'key=XXX' and copy XXX below
spreadsheet_key = 'XXX'
# All spreadsheets have worksheets. I think worksheet #1 by default always
# has a value of 'od6'
worksheet_id = 'od6'

spr_client = gdata.spreadsheet.service.SpreadsheetsService()
spr_client.email = email
spr_client.password = password
spr_client.source = 'WaterBot Data Logger Script'
spr_client.ProgrammaticLogin()

# Prepare the dictionary to write
dict = {}
dict['timestamp'] = time.strftime('%Y/%m/%d %H:%M:%S')
dict['humidity'] = humidity
dict['temperature'] = temperature
dict['light'] = light
dict['pumpstate'] = pumpstate
#print dict

entry = spr_client.InsertRow(dict, spreadsheet_key, worksheet_id)
#if isinstance(entry, gdata.spreadsheet.SpreadsheetsList):
#  print "Insert row succeeded."
#else:
#  print "Insert row failed."
  
#from time import sleep
#sleep(5)
